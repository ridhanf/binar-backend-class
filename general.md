>  Resources for learning code

### All about code
-   [Refactoring Guru | Refactoring and Design Pattern](https://refactoring.guru/)
-   [Tutorial Points | Design Patterns](https://www.tutorialspoint.com/design_pattern)
-   [Geeks for Geeks | Data Structure and Algorithm](https://www.geeksforgeeks.org/data-structures/)
-   [Sourcemaking | Software Arcitecture](https://sourcemaking.com/)
-   [Thoughbot | S.O.L.I.D principles](https://robots.thoughtbot.com/back-to-basics-solid)